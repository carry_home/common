package com.carry.common.file;

import com.carry.common.time.TimeString;

import cn.hutool.core.io.file.FileNameUtil;

/**
 * 文件名产生
 * @author CARRY
 * @Date 2020年11月6日上午2:10:30
 */
public class FileName {

	/**
	 * 产生一个时间字符串加上三位随机数的文件名称，类似20201106014131283423.jpg
	 *@param 传入文件名，带后缀
	 * @return
	 */
	public static String builder(String fileName) {
		StringBuilder sb = new StringBuilder();
		sb.append(TimeString.buildRandom(3));
		sb.append(".");
		sb.append(FileNameUtil.extName(fileName));
		
		return sb.toString();
	}
}
