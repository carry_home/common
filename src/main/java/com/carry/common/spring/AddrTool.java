package com.carry.common.spring;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * spring程序获取ip地址工具类
 * @author CARRY ME HOME
 * 2020-7-179:27:14
 */
public class AddrTool {

	public static String getAddr() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		
		return request.getRemoteAddr();
	}
	

}
