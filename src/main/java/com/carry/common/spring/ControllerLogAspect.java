package com.carry.common.spring;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 切controller方法，方便调试别人的项目代码
 * 
 * @author CARRY ME HOME 2020年7月15日下午6:58:36
 */
@Aspect
@Component
public class ControllerLogAspect {

	@Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void requestPoint() {
	}

	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
	public void getPoint() {
	}

	@Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
	public void postPoint() {
	}

	@Pointcut("@annotation(org.springframework.web.bind.annotation.PutMapping)")
	public void putPoint() {
	}

	@Pointcut("@annotation(org.springframework.web.bind.annotation.DeleteMapping)")
	public void deletePoint() {
	}

	@Before(value = "requestPoint() || getPoint() || postPoint() || putPoint() || deletePoint()")
	public void logController(JoinPoint joinPoint) {
		System.err.println(String.format("=v= %s - %s - %s", joinPoint.getSignature(), AddrTool.getAddr(), LocalDateTime.now()));
	}

}
