package com.carry.common.time;

import java.util.Date;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;

/**
 * 时间字符串产生
 * @author CARRY
 * @Date 2020年11月6日上午1:43:13
 */
public class TimeString {
	
	/**
	 * 基于当前时间返回一个yyyyMMddHHmmssSSS格式的时间字符串，类似2020110601413128
	 * @return
	 */
	public static String build() {
		StringBuffer sb = new StringBuffer();
		sb.append(DateUtil.format(new Date(), "yyyyMMddHHmmssSSS"));
		
		return sb.toString();
	}
	
	/**
	 * 返回一个基于当前时间的时间字符串并加上N位数的随机数,类似20201106014131283423
	 *
	 * @return
	 */
	public static String buildRandom(int n) {
		StringBuffer sb = new StringBuffer();
		sb.append(DateUtil.format(new Date(), "yyyyMMddHHmmssSSS"));
		sb.append(RandomUtil.randomNumbers(n));
		
		return sb.toString();
	}

}
