package com.carry.common.tool;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import cn.hutool.core.collection.CollectionUtil;

/**
 * @author CARRY
 * @Date 2020年11月6日下午4:46:35
 */
public class ListTool {

	/**
	 * 去除list中的null元素
	 *
	 * @param list
	 */
	public static void removeNull(List<?> list) {
		if (CollectionUtil.isEmpty(list)) {
			return;
		}

		list.removeAll(Collections.singleton(null));
	}

	/**
	 * 判断list中是否有重复元素，如果有则返回true。 <br>
	 * 注意： 不能使用Arrays.asList生成的List进行去重
	 * 
	 * @param <T>
	 *
	 * @param list
	 * @param remove 设置为true则会对list进行去重
	 * @return
	 */
	public static <T> boolean checkDuplicateList(List<T> list, boolean remove) {
		if (CollectionUtil.isEmpty(list)) {
			return false;
		}

		LinkedHashSet<T> set = new LinkedHashSet<>(list.size());
		boolean d = list.size() != set.size() ? true : false;
		;

		if (remove) {
			set.addAll(list);
			list.clear();
			list.addAll(set);
		}

		return d;
	}
}
