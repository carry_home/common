package com.carry.common.tool;

import org.apache.logging.log4j.util.Strings;

import cn.hutool.extra.pinyin.PinyinUtil;

/**
 * @author CARRY
 * @Date 2020年11月6日下午4:46:46
 */
public class StringTool {

	/**
	 * 获取名称的首字母，如果是中文则返回拼音的首字母。如果既不是中文也不是英文开头的名称，则返回#号
	 *
	 * @param name
	 * @return
	 */
	public static String getInitial(String name) {
		char c = PinyinUtil.getPinyin(name).charAt(0);

		if (Character.isLetter(c)) {
			return Strings.toRootUpperCase(String.valueOf(c));
		} else {
			return "#";
		}

	}
}
